Theo Sandstrom

Turner 7-6

January 9, 2014

Pont Draft

The Pont Du Gard is an ancient Roman aqueduct bridge in south-east
France. An aqueduct is a long pipe or canal that uses gravity to
transport water. Pont Du Gard is part of the
[[http://en.wikipedia.org/wiki/N%C3%AEmes][Nîmes]] aqueduct, a structure
designed to carry water over a 50 km distance from a spring at
Uz[[http://en.wikipedia.org/wiki/Uz%C3%A8s][ès]] to Nemausus, a Roman
colony. The aqueduct carried about 44,000,000 imperial gallons of water
every day. In the top level of the bridge, there was a small tunnel that
the water travelled through. Since the path the aqueduct takes is very
hilly, most of it is underground, but part of the aqueduct has to cross
the gorge of the Gargon. This crossing required that the aqueduct bridge
be constructed. The aqueduct bridge's construction was completed around
50 AD, after 15 years of construction. The construction of Pont Du Gard
employed 800-1,000 workers, and cost 30,000,000 sesterces, the ancient
Roman monetary unit. This was enough to by 60,000 donkeys at the time.
The Pont Du Gard bridge was constructed almost completely out of
limestone. The limestone was cut into perfectly shaped blocks, so that
the bridge could be supported by friction alone, eliminating the need
for mortar. The bridge used about 50,400 tons of limestone. Some of the
individual blocks could weigh up to 6 tons. The construction of this
aqueduct bridge is credited to Marcus Vipsanius Agrippa, the son-in-law
of Augustus. Agrippa was the senior magistrate in charge of providing
water for Rome and its colonies. The bridge was built very precisely
using ancient Roman technology: over the entire length of the bridge,
the height only drops 2.5 centimeters. The Pont Du Gard is an amazing
structure built by the Romans. It was built very precisely, did its job,
and looks amazing.

My model of an aqueduct bridge is made of wood. I thought for a while
about what materials I should use to make the aqueduct bridge. I was
planning on using foam, but then I decided that wood might look better
on the finished project. Before I started the model, I found a piece of
wood to work with in the garage. Then I went to a hardware store and got
the paint, sandpaper, and foam that I knew I would need to make the
model. I started with a 1¼ inch sheet of wood, and cut it to the size of
the bottom level. Then I used a drill to make holes in it, and used a
saw to cut away part of the circle, leaving an arch. I sanded the inside
of the arch, and painted it, completing the bottom level. Then I used
the same process on ¾ inch wood to make the upper level. The only
difference was the size of the level, and the size of the arches. I also
drilled a small hole in the upper level to show where the water would
travel. After completing the drilling, I glued the two levels together,
and used screws to attach them to a wooden board. I painted the board
and the bridge with brown paint, just to add a bit more color. Then I
used spray-on foam insulation to create mountains on either side of the
bridge. I let the foam dry, and painted it brown as well. The whole
project took me about 4 to 5 days, but a lot of that time was spent
letting glue, paint, or foam dry. I think that making this project was a
great way to learn about the ancient Romans. I had a lot of fun
researching aqueducts, planning my project, and putting it together.

[[file:media/image4.jpg]]

Above is a picture of the Pont Du Gard aqueduct bridge

[[file:media/image2.jpg]]

Above is a picture of the inside of the

water transport pipe inside the bridge

Source: en.wikipedia.org
