Theo Sandstrom

Barrett 7-9

June 18, 2014

Miracle Worker OR Reflection

Earlier this year, our class read /The Miracle Worker,/ by William
Gipson. This is the story of Helen Keller, who was blind and deaf. After
we finished, everybody wrote an open response about how Annie Sullivan
was able to teach Helen language. I chose to include this assignment in
my portfolio because it was one of the first open responses that we
wrote in 7th grade, and I was proud of it. It was also interesting to
think about what makes a good teacher. I think that this is a good
assignment that every 7th grader should have, because it forces you to
realize that your favorite teacher, who is nice and funny, is not always
a good teacher. It makes you realize that you should be thankful for
your strict teachers.
