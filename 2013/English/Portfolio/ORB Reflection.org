Theo Sandstrom

Barrett 7-9

June 13, 2014

ORB Reflection

My favorite outside reading book I have read this year is Orson Scott
Card's /Ender's Game./ This is the story of a boy named Ender living far
in the future. In this time, humans on Earth are preparing for a massive
battle against the Buggers, a strange alien race that nobody completely
understands. As one of the smartest boys in his battle training group,
Ender becomes the leader of the army that will eventually be faced with
the task of wiping out all buggers in the universe. But Ender soon
realizes that the Buggers may not be what they seem, and that they
probably don't want to have a war with the humans. I really enjoyed
reading this book because it was always exciting, and I never felt
bored. I once spent an entire day reading, because I got completely
sucked into the story. I am glad that I chose to read this book, because
it helped me realize that it can be very easy to make assumptions about
people, and have small misunderstandings that can have huge
consequences.
