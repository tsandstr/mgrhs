Ancient Chinese Writing

In Ancient China, writing had many uses and meanings. As the British
Museum website says, different inscriptions showed different values on
Chinese coins. The Chinese language is expressed in characters, or a
symbol used to represent a sound, word or idea. The material that was
used to write on had an effect on what information was being written.
Some of the materials used were bone, bronze, and bamboo. The Chinese
would inscribe words and characters onto bronze pots to pay respect to
their king or to their deceased relatives. Wooden slips were also
commonly used for writing on. They contained information such as medical
remedies and military tactics. Wooden slips were tied to others and then
rolled to make them easier to carry. They would be tied with string and
then sealed with clay. Another writing material used was the shoulder
blade of an oxen, also called the “oracle bone.” Oracle bones show us
some of the earliest writing in China and gave us information on the
Shang Dynasty. With this writing material Chinese writing could become
more expressive. It came to be known as "calligraphy". Calligraphy is
seen as an art form. With With calligraphy on so many specimens it is
easy to find information on writing in Ancient China. Coming from a rich
history, Chinese writing is now a beautiful way to express words. (A)

[[file:media/image2.jpg]]
