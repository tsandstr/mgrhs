#+BEGIN_QUOTE
  Theo Sandstrom

  Kaiser 7-3

  Tuesday January 7, 2014

  History Of Writing- Roman Britain

  History of Writing- Roman Britain

  By studying writing in Roman Britain, paleoanthropologists can learn
  how the Romans used writing, what materials they used, and how they
  wrote. Also, by reading ancient tablets, scientists can learn what the
  Romans thought, and what important events were occurring.
  Paleontologists know that in some civilizations, writing was used only
  for government, administrative, and religious purpose, but according
  to the British Museum[fn:1] and ancientcivilizations.co.uk[fn:2], the
  Romans wrote down personal information, and letters. Many people
  learned how to write, including men and women, and even those who were
  not citizens of Rome. One important source of information for studying
  Roman Britain is the Vindolanda tablets. Written in the second century
  AD, these tablets are the oldest handwritten documents in Britain.
  Fort Vindolanda was one of the main Roman military outposts. These
  tablets teach us a lot about Roman cursive writing, and what was
  written down. Also, the Vindolanda tablets can teach us about the
  Roman military. Since several of the tablets were personal letters
  between soldiers, we can see what the soldiers thought about the
  military, Rome, and life in general during the second century.
  Paleontologists have also learned what materials the Romans used to
  write: wooden boards, carbon ink, and quill-like pens. According to
  wikipedia, carbon ink is a black ink made from mixing soot, and a
  binding agent such as animal glue. By studying the tablets from
  Vindolanda, and other tablets, paleontologists can learn about Roman
  culture, military, and economics. Scientists can learn about the lives
  of common people and soldiers to find out more about the past
#+END_QUOTE

[[file:media/image2.jpg]]

Above is a picture of the Vindolanda

tablets.^{1}

[fn:1] "British Museum - The Vindolanda tablets." 2007. 9 Jan. 2014
       <[[http://www.britishmuseum.org/explore/online_tours/britain/our_top_ten_british_treasures/the_vindolanda_tablets.aspx][http://www.britishmuseum.org/explore/online\_tours/britain/our\_top\_ten\_british\_treasures/the\_vindolanda\_tablets.aspx]]>

[fn:2] "Ancient Civilizations." 2003. 9 Jan. 2014
       <[[http://www.ancientcivilizations.co.uk/][http://www.ancientcivilizations.co.uk/]]>
