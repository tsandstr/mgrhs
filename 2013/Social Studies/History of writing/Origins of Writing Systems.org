Theo SandstromKaiser 7-3

January 17, 2014

History of Writing

History of Writing

Writing has helped preserve stories and information about politics,
economics, and the life of citizens from thousands of years ago.
Different writing systems all over the world were used for several
similar reasons, including story-telling, remembering information and
communicating. By studying the evolution of writing, we can learn about
people's needs, and by analyzing what ancient civilizations wrote down,
we can learn about what was happening in history. Everything from major
political events to a soldiers birthday to myths and legends can be
learned by analyzing the scripts from ancient civilizations can and have
been written down.

* Roman Britain
  :PROPERTIES:
  :CUSTOM_ID: roman-britain
  :END:

By studying writing in Roman Britain, paleoanthropologists can learn how
the Romans used writing, what materials they used, and how they wrote.
Also, by reading ancient tablets, scientists can learn what the Romans
thought, and what important events were occurring. Paleontologists know
that in some civilizations, writing was used only for government,
administrative, and religious purpose, but according to the British
Museum and Ancient Civilizations, the Romans wrote down personal
information, and letters. Many people learned how to write, including
men and women, and even those who were not citizens of Rome. One
important source of information for studying Roman Britain is the
Vindolanda tablets. Written in the second century AD, these tablets are
the oldest handwritten documents in Britain. Fort Vindolanda was one of
the main Roman military outposts. These tablets teach us a lot about
Roman cursive writing, and what was written down. Also, the Vindolanda
tablets can teach us about the Roman military. Since several of the
tablets were personal letters between soldiers, we can see what the
soldiers thought about the military, Rome, and life in general during
the second century. Paleontologists have also learned what materials the
Romans used to write: wooden boards, carbon ink, and quill-like pens.
According to wikipedia, carbon ink is a black ink made from mixing soot,
and a binding agent such as animal glue. By studying the tablets from
Vindolanda, and other tablets, paleontologists can learn about Roman
culture, military, and economics. Scientists can learn about the lives
of common people and soldiers to find out more about the
past.[fn:1] [fn:2]

[[file:media/image4.jpg]]

Above is an image of the Vindolanda\

Tablets

* Phoenician
  :PROPERTIES:
  :CUSTOM_ID: phoenician
  :END:

The Phoenician alphabet had many interesting aspects such as influencing
many languages, being spread around the world, and only having twenty
two characters. One detail ,according to the british museum, about the
Phoenician alphabet is that it influenced languages like Aramaic,
Hebrew, Ammonite, Moabic, and Edomite. Those languages all have
characters related to ones in the Phoenician alphabet. Another
interesting detail from the British Museum is the way the language was
able to spread around the world and influence other languages. The main
way it was spread was through trade and commerce around the
Mediterranean. The Greeks also helped spread it. In addition, one detail
about the alphabet is that it only had twenty two characters.The
alphabet had no vowels. The Greeks later adopted the alphabet, they
added vowels. The Phoenicians also used the same writing system as the
Canaanites. The way the language was created in order to record daily
tasks. In conclusion, when you add all of these reasons up, you can see
that the Phoenicians played a very important role in forming alphabets
and influencing writing all around the world.[fn:3] [fn:4]

[[file:media/image3.jpg]]

Above is an image of a

Phoenician Tablet

* Indus
  :PROPERTIES:
  :CUSTOM_ID: indus
  :END:

The Indus Valley scripts are some of the most interesting scripts found.
These scripts have been found all over the Indus Valley region. In
temples, homes, and in important buildings. These scripts were primarily
written on clay, metal, and smoothed stone. On these scripts there are
many pictures of animals like monkeys, bison, bulls, deer,and important
looking humans. There is also undeciphered text above some of the
pictures.These scripts are from one of the ancient world's biggest
civilizations but they have yet to be deciphered. No matter how much
time, and effort put into this cause it has stumped all of its
opponents. People have guessed that the scripts are about religion,
trade, or government. The reason they haven't been deciphered is the
Indus Valley scripts are not usually made to represent a text or script
making so they can't try to figure out the text by knowing one symbol
and logically deducing the next one. Also, In a line from a script there
is no saying if one symbol has any relation to the symbol next to it.
Finally, there are no scripts or languages that are like this so they
can't compare and figure out what the symbols might mean. We may not
know very much about these scripts, but we have made some progress.
Scientists do know that these scripts ran from right to left and then
left to right and then right to left once again.(Like a snake).
Scientists are slowly deducing what the seals mean through careful
examination of scripts and what symbols lay on them. We are figuring
more about these scripts everyday but we are still long way from
deciphering these texts.[fn:5] [fn:6] [fn:7] [fn:8]

[[file:media/image6.jpg]]

Above is an image of an Indus script

Writing is used all over the world, and it has been for thousands of
years. It is a way for civilizations to remember their important
information, communicate with each other, and tell stories. Writing is
probably the most important invention that man has made, and we will
probably be using writing forever.

[fn:1] "Ancient Civilizations." 2003. 17 Jan. 2014
       <[[http://www.ancientcivilizations.co.uk/][http://www.ancientcivilizations.co.uk/]]>

[fn:2] "British Museum - Welcome to the British Museum." 2004. 17 Jan.
       2014
       <[[http://www.britishmuseum.org/][http://www.britishmuseum.org/]]>

[fn:3] "British Museum - Welcome to the British Museum." 2004. 17 Jan.
       2014
       <[[http://www.britishmuseum.org/][http://www.britishmuseum.org/]]>

[fn:4] “History of Writing” Adam Cohen

[fn:5] "British Museum - Welcome to the British Museum." 2004. 17 Jan.
       2014
       <[[http://www.britishmuseum.org/][http://www.britishmuseum.org/]]>

[fn:6] "BBC - Homepage." 17 Jan. 2014
       <[[http://www.bbc.co.uk/][http://www.bbc.co.uk/]]>

[fn:7] "Blog of the Long Now." 2007. 17 Jan. 2014
       <[[http://blog.longnow.org/][http://blog.longnow.org/]]>

[fn:8] "Indus Scripts” David Falk
