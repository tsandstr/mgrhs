*Hadrosaurs!*

*What does the physical structure tell us about their niche(food
sources, role in environments, etc.)*

Hadrosaurs were herbivores, meaning that they don't eat meat. Hadrosaurs
had a beak like face used to clip leaves and twigs. They had thousands
of cheek teeth used to grind food before swallowing. Their jaw and
chewing method show that they probably ate vegetation on or close to the
ground.[fn:1]

*What does the physical structure tell us about movement of the
organism?*

Hadrosaurs had a giant body balanced on stout legs by a thick tail. This
shows that they ran bipedally using their tails for balance. We think
that they were usually quadrupedal, but ran bipedally. By analyzing the
muscles and tendons of a hadrosaur, scientists have calculated that some
hadrosaurs ran as fast as 28 miles per hour, outrunning many other
dinosaurs, including Tyrannosaurus rex, which runs at only 20 miles per
hour.[fn:2]

*What does the physical structure tell us about the relationship of this
organism with other organisms (both extinct and extant)*

Hadrosaurs were related to other dinosaur species of the Mesozoic. They
were very close relatives of the iguanodontid, and they were probably
related. Also, since they laid eggs, they are probably related to
today's reptiles and birds. Some of the close relatives of hadrosaurs
are thescelosaurus and tenontosaurus.[fn:3] Thescelosaurus also had a
long beak similar to those of hadrosaurs.[fn:4]

*What type of fossils of your organism have been found? What information
did scientists get from the fossils?*

Hadrosaur fossils are some of the most common dinosaur fossils found.
Many different fossils have been found, including eggs, juveniles, and
adults. We have complete skeletons from several hadrosaurs, as well as
mummified bodies, skin impressions, stomach contents, footprints, and
some internal organs.

*What don't we know about the organism? What Hypotheses have been made?*

Although we know a lot about hadrosaurs, there are still some things we
have yet to learn. One of the things we don't know is how they moved. We
think that hadrosaurs were mainly quadrupedal, but ran bipedally.
Although this is a good hypothesis based on the information available,
we are not positive that this is how they moved. Another thing that we
are unsure of is the purpose of the crest on

the head of some hadrosaurs. We have several hypotheses: the crest could
have acted like a snorkel so that hadrosaurs could breathe underwater,
they could be used to warm up the air before it went to the hadrosaurs
lungs, it could have been used to hold air when the dinosaur went
underwater, or it could be used to make loud noises.[fn:5]

*When does the organism appear in geologic time?*

Hadrosaurs lived in the Phanerozoic eon, Mesozoic Era, Cretaceous
period.

*When does the organism go extinct? What do we know or hypothesize may
have caused its extinction?*

Hadrosaurs lived right up until the end of the Cretaceous period. Since
asteroids hit the Earth right at the time when the hadrosaurs died out,
scientists can safely assume that the hadrosaurs were killed by the
asteroids. We know that asteroids hit the Earth at the end of the
Cretaceous, because there are thin layers of a rare element called
iridium in the ground from that time period. Iridium is very rare in the
Earth, but is very common in asteroids.

*Where on Earth have fossils of these organisms found?*

Fossils of hadrosaurs have been found on all continents, but are
primarily on Asia, Europe, and North America.

*Works Cited*

http://www.[[http://opendino.wordpress.com/author/andyfarke/][opendino.wordpress.com]]
(phylogenetic tree)

[[http://hdwallpaperspictures.com]] (map)

American Museum of Natural History

[fn:1] "Hadrosaurid - Wikipedia, the free encyclopedia." 2005. 5 Dec.
       2013
       <[[http://en.wikipedia.org/wiki/Hadrosaurid][http://en.wikipedia.org/wiki/Hadrosaurid]]>

[fn:2] "Dakota (fossil) - Wikipedia, the free encyclopedia." 2007. 5
       Dec. 2013
       <[[http://en.wikipedia.org/wiki/Dakota_(fossil)][http://en.wikipedia.org/wiki/Dakota\_(fossil)]]>

[fn:3] "UNM - New Mexico's Flagship University | The University of New
       ..." 5 Dec. 2013 <[[http://www.unm.edu/][http://www.unm.edu/]]>

[fn:4] "Thescelosaurus - Wikipedia, the free encyclopedia." 2005. 5 Dec.
       2013
       <[[http://en.wikipedia.org/wiki/Thescelosaurus][http://en.wikipedia.org/wiki/Thescelosaurus]]>

[fn:5] "Hadrosaurs." 5 Dec. 2013
       <[[http://www.ucmp.berkeley.edu/diapsids/ornithischia/hadrosauria.html][http://www.ucmp.berkeley.edu/diapsids/ornithischia/hadrosauria.html]]>
